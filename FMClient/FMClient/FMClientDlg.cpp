// FMClientDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMClient.h"
#include "FMClientDlg.h"

#include "FMClientUnit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CFMClientDlg 对话框

CFMClientDlg::CFMClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFMClientDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CFMClientDlg::~CFMClientDlg()
{
	int i, j;
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			CFMClientUnit* pu = m_aryUnitGroup[j][i];
			delete pu;
		}
	}
}

void CFMClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHK_REPRESS_MSGBOX, m_chkRepressMsgBox);
}

BEGIN_MESSAGE_MAP(CFMClientDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CFMClientDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CHK_ECG, &CFMClientDlg::OnBnClickedChkEcg)
	ON_BN_CLICKED(IDC_CHK_SPO2, &CFMClientDlg::OnBnClickedChkSpo2)
	ON_BN_CLICKED(IDC_CHK_NIBP, &CFMClientDlg::OnBnClickedChkNibp)
	ON_BN_CLICKED(IDC_CHK_TEMP, &CFMClientDlg::OnBnClickedChkTemp)
	ON_BN_CLICKED(IDC_CHK_FETUS, &CFMClientDlg::OnBnClickedChkFetus)
	ON_BN_CLICKED(ID_BTN_START_ALL, &CFMClientDlg::OnBnClickedBtnStartAll)
	ON_BN_CLICKED(ID_BTN_STOP_ALL, &CFMClientDlg::OnBnClickedBtnStopAll)
	ON_BN_CLICKED(ID_BTN_SELECT_ALL, &CFMClientDlg::OnBnClickedBtnSelectAll)
	ON_BN_CLICKED(ID_BTN_CLEAR_ALL, &CFMClientDlg::OnBnClickedBtnClearAll)
	ON_BN_CLICKED(IDC_CHK_FETUS, &CFMClientDlg::OnBnClickedChkFetus)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHK_REPRESS_MSGBOX, &CFMClientDlg::OnBnClickedChkRepressMsgbox)
END_MESSAGE_MAP()


// CFMClientDlg 消息处理程序

BOOL CFMClientDlg::OnInitDialog()
{
	int i, j, num;

	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	//ShowWindow(SW_MINIMIZE);

	// TODO: 在此添加额外的初始化代码
	
	// 创建子窗口
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			num = UNIT_COL_COUNT * j + i + 1;
			CFMClientUnit* pu = new CFMClientUnit(this);
			m_aryUnitGroup[j][i] = pu;
			pu->Create(num);
			pu->MoveWindow(i*UNIT_COL_SPAN, j*UNIT_ROW_SPAN+UNIT_MARGIN, UNIT_COL_SPAN, UNIT_ROW_SPAN);
		}
	}
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CFMClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CFMClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CFMClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CFMClientDlg::OnBnClickedOk()
{
	CDialog::OnOK();
}


void CFMClientDlg::OnBnClickedChkEcg()
{
	int i, j, value;
	CButton* pcb = (CButton*)GetDlgItem(IDC_CHK_ECG);
	value = pcb->GetCheck();
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			CFMClientUnit* pu = m_aryUnitGroup[j][i];
			if (!pu->IsRunning()) {
				pu->m_chkEcg.SetCheck(value);
			}
		}
	}
}


void CFMClientDlg::OnBnClickedChkSpo2()
{
	int i, j, value;
	CButton* pcb = (CButton*)GetDlgItem(IDC_CHK_SPO2);
	value = pcb->GetCheck();
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			CFMClientUnit* pu = m_aryUnitGroup[j][i];
			if (!pu->IsRunning()) {
				pu->m_chkSpo2.SetCheck(value);
			}
		}
	}
}


void CFMClientDlg::OnBnClickedChkNibp()
{
	int i, j, value;
	CButton* pcb = (CButton*)GetDlgItem(IDC_CHK_NIBP);
	value = pcb->GetCheck();
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			CFMClientUnit* pu = m_aryUnitGroup[j][i];
			if (!pu->IsRunning()) {
				pu->m_chkNibp.SetCheck(value);
			}
		}
	}
}


void CFMClientDlg::OnBnClickedChkTemp()
{
	int i, j, value;
	CButton* pcb = (CButton*)GetDlgItem(IDC_CHK_TEMP);
	value = pcb->GetCheck();
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			CFMClientUnit* pu = m_aryUnitGroup[j][i];
			if (!pu->IsRunning()) {
				pu->m_chkTemp.SetCheck(value);
			}
		}
	}
}


void CFMClientDlg::OnBnClickedChkFetus()
{
	int i, j, value;
	CButton* pcb = (CButton*)GetDlgItem(IDC_CHK_FETUS);
	value = pcb->GetCheck();
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			CFMClientUnit* pu = m_aryUnitGroup[j][i];
			if (!pu->IsRunning()) {
				pu->m_chkFetus.SetCheck(value);
			}
		}
	}
}

// 全部开始
void CFMClientDlg::OnBnClickedBtnStartAll()
{
	int i, j;
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			CFMClientUnit* pu = m_aryUnitGroup[j][i];
			if (!pu->IsRunning()) {
				pu->Start();
			}
		}
	}
}

// 全部停止
void CFMClientDlg::OnBnClickedBtnStopAll()
{
	int i, j;
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			CFMClientUnit* pu = m_aryUnitGroup[j][i];
			if (pu->IsRunning()) {
				pu->Stop();
			}
		}
	}
}

void CFMClientDlg::OnBnClickedBtnSelectAll()
{
	int i, j;
	((CButton*)GetDlgItem(IDC_CHK_ECG))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_CHK_SPO2))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_CHK_NIBP))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_CHK_TEMP))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_CHK_FETUS))->SetCheck(1);
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			CFMClientUnit* pu = m_aryUnitGroup[j][i];
			if (!pu->IsRunning()) {
				pu->m_chkEcg.SetCheck(1);
				pu->m_chkSpo2.SetCheck(1);
				pu->m_chkNibp.SetCheck(1);
				pu->m_chkTemp.SetCheck(1);
				pu->m_chkFetus.SetCheck(1);
			}
		}
	}
}


void CFMClientDlg::OnBnClickedBtnClearAll()
{
	int i, j;
	((CButton*)GetDlgItem(IDC_CHK_ECG))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_CHK_SPO2))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_CHK_NIBP))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_CHK_TEMP))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_CHK_FETUS))->SetCheck(0);
	for (j=0; j<UNIT_ROW_COUNT; j++) {
		for (i=0; i<UNIT_COL_COUNT; i++) {
			CFMClientUnit* pu = m_aryUnitGroup[j][i];
			if (!pu->IsRunning()) {
				pu->m_chkEcg.SetCheck(0);
				pu->m_chkSpo2.SetCheck(0);
				pu->m_chkNibp.SetCheck(0);
				pu->m_chkTemp.SetCheck(0);
				pu->m_chkFetus.SetCheck(0);
			}
		}
	}
}

int CFMClientDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	::SetProp(this->m_hWnd, ::AfxGetAppName(), (HANDLE)1);
	return 0;
}

void CFMClientDlg::OnDestroy()
{
	CDialog::OnDestroy();
	::RemoveProp(this->m_hWnd, ::AfxGetAppName());
}

void CFMClientDlg::OnBnClickedChkRepressMsgbox()
{
	CFMClientUnit::m_bRepressMsgBox = (BOOL)m_chkRepressMsgBox.GetCheck();
}
