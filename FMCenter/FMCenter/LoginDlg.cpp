// LoginDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "LoginDlg.h"
#include "afxdialogex.h"


// CLoginDlg 对话框

IMPLEMENT_DYNAMIC(CLoginDlg, CDialogEx)

CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLoginDlg::IDD, pParent)
	, m_db(theApp.m_db)
	, m_dbLock(theApp.m_csDBLock)
{
	m_arAdminGroup = NULL;
}

CLoginDlg::~CLoginDlg()
{
	if (m_arAdminGroup) {
		delete[] m_arAdminGroup;
		m_arAdminGroup = NULL;
	}
}

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDT_ADMIN_NAME, m_edtLoginName);
	DDX_Control(pDX, IDC_EDT_PASSWORD, m_edtLoginPass);
}

BEGIN_MESSAGE_MAP(CLoginDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CLoginDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CLoginDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CLoginDlg 消息处理程序
BOOL CLoginDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	QueryAdminInfo();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CLoginDlg::QueryAdminInfo()
{
	// 执行查询
	CRecordset rs(&m_db);
	CString sWhereStatement;
	CString sql;

	sql = _T("SELECT oid,otype,oname,opass FROM operator_info");

	OutputDebugString(sql + _T("\n"));

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CLoginDlg::QueryAdminInfo第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}

	try {
		while (! rs.IsEOF()) {
			rs.MoveNext();
		}
		m_nAdminCount = rs.GetRecordCount();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CLoginDlg::QueryAdminInfo第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		m_nAdminCount = 0;
		return;
	}
	
	if (0 == m_nAdminCount) {
		m_nAdminCount = 1;
		m_arAdminGroup = new ADMIN_INFO[1];
		m_arAdminGroup[0].id = -1;  //初始用户
		m_arAdminGroup[0].nType = 1;//高级管理员
		m_arAdminGroup[0].sName = _T("admin");
		m_arAdminGroup[0].sPass = _T("admin");
		goto fini;
	}

	m_nAdminCount ++;//第一个留给默认密码使用
	m_arAdminGroup = new ADMIN_INFO[m_nAdminCount];
	int nAdvancedAcminCount = 0;

	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CLoginDlg::QueryAdminInfo第3个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}
	int i = 1;
	while (!rs.IsEOF()) {
		
		try {
			CString sValue;
			CDBVariant var;

			rs.GetFieldValue(_T("oid"), var);
			m_arAdminGroup[i].id = var.m_lVal;

			rs.GetFieldValue(_T("otype"), var);
			m_arAdminGroup[i].nType = var.m_lVal;
			if (1 == var.m_lVal) {
				nAdvancedAcminCount ++;
			}

			rs.GetFieldValue(_T("oname"), sValue);
			m_arAdminGroup[i].sName = sValue;

			rs.GetFieldValue(_T("opass"), sValue);
			m_arAdminGroup[i].sPass = sValue;
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CLoginDlg::QueryAdminInfo第4个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return;
		}
		rs.MoveNext();
		i++;
	}

	if (nAdvancedAcminCount > 0) {
		m_arAdminGroup[0].sName.Empty();
	}
	else {
		m_arAdminGroup[0].id = -1;  //初始用户
		m_arAdminGroup[0].nType = 1;//高级管理员
		m_arAdminGroup[0].sName = _T("admin");
		m_arAdminGroup[0].sPass = _T("admin");
	}

fini:

	if (rs.IsOpen()) {
		rs.Close();
	}
}

void CLoginDlg::OnBnClickedOk()
{
	CString sName;
	CString sPass;
	m_edtLoginName.GetWindowText(sName);
	m_edtLoginPass.GetWindowText(sPass);

	int i;
	for (i=0; i<m_nAdminCount; i++) {
		if (m_arAdminGroup[i].sName.IsEmpty()) {
			continue;
		}
		if (sName == m_arAdminGroup[i].sName &&
			sPass == m_arAdminGroup[i].sPass) {
			theApp.m_nAdminID = m_arAdminGroup[i].id;
			theApp.m_sAdminName = m_arAdminGroup[i].sName;
			theApp.m_nAdminType = m_arAdminGroup[i].nType;
			CDialogEx::OnOK();
			return;
		}
	}

	MessageBox(_T("用户名密码错误！"));
}

void CLoginDlg::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}
