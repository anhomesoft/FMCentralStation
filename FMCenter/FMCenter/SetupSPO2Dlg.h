#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CSetupSPO2Dlg 对话框
class CFMRecordUnit;
class CSetupSPO2Dlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSetupSPO2Dlg)

private:
	CFMRecordUnit* m_pru;

public:
	CSetupSPO2Dlg(CWnd* pParent, CFMRecordUnit* pru);
	virtual ~CSetupSPO2Dlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_SETUP_SPO2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edtSPO2High;
	CEdit m_edtSPO2Low;
	CEdit m_edtPRHigh;
	CEdit m_edtPRLow;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
