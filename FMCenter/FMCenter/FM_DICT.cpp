#include "stdafx.h"
#include "FM_DICT.h"


TCHAR* CFMDict::m_dictOnOff[] = {
	_T("关"),
	_T("开")
};

TCHAR* CFMDict::m_dictFHRPaperSpeedOpt[] = {
	_T("3CM/MIN"),
	_T("1CM/MIN")
};

TCHAR* CFMDict::m_dictGender[] = {
	_T("男"),
	_T("女")
};

TCHAR* CFMDict::m_dictPatientType[] = {
	_T("成人"),
	_T("小儿"),
	_T("新生儿")
};

TCHAR* CFMDict::m_dictBloodType[] = {
	_T("不祥"),
	_T("AB"),
	_T("A"),
	_T("B"),
	_T("O")
};

TCHAR* CFMDict::m_dictAdminType[] = {
	_T(""),
	_T("高级")
};

TCHAR* CFMDict::m_dictECGLead[] = {
	_T("I"),
	_T("II"),
	_T("III"),
	_T("AVR"),
	_T("AVL"),
	_T("AVF"),
	_T("V")
};

TCHAR* CFMDict::m_dictECGSpeed[] = {
	_T("25mm/s"),
	_T("50mm/s"),
	_T("12.5mm/s")
};

TCHAR* CFMDict::m_dictECGFilter[] = {
	_T("监护"),
	_T("手术"),
	_T("诊断")
};

TCHAR* CFMDict::m_dictECGGain[] = {
	_T("x 0.25"),
	_T("x 0.5"),
	_T("x 1"),
	_T("x 2"),
	_T("自动")
};

TCHAR* CFMDict::m_dictNIBPMode[] = {
	_T("手动"),
	_T("1分钟自动"),
	_T("2分钟自动"),
	_T("3分钟自动"),
	_T("4分钟自动"),
	_T("5分钟自动"),
	_T("10分钟自动"),
	_T("15分钟自动"),
	_T("30分钟自动"),
	_T("60分钟自动"),
	_T("90分钟自动"),
	_T("120分钟自动"),
	_T("180分钟自动"),
	_T("240分钟自动"),
	_T("480分钟自动")
};

TCHAR* CFMDict::m_dictNIBPUnit[] = {
	_T("mmHg"),
	_T("kPa")
};

TCHAR* CFMDict::m_dictTEMPUnit[] = {
	_T("摄氏度℃"),
	_T("华氏度℉")
};

TCHAR* CFMDict::m_dictFHRReportType[] = {
	_T("10分钟"),
	_T("20分钟")
};

TCHAR* CFMDict::m_dictDoctorMark[] = {
	_T(""),
	_T("翻身"),
	_T("左侧卧位"),
	_T("右侧卧位"),
	_T("半侧卧位"),
	_T("滴注催产素"),
	_T("停滴催产素"),
	_T("调整腹带及探头"),
	_T("阴道检查"),
	_T("暂停"),
	_T("去卫生间"),
	_T("滴注葡萄糖"),
	_T("滴注液体"),
	_T("吸氧"),
	_T("停止吸氧"),
	_T("进食")
};

TCHAR* CFMDict::m_dictECGArrMode[] = {
	_T(""),
	_T("停搏"),
	_T("室颤/室速"),
	_T("R ON T"),
	_T("多发室早"),
	_T("两个室早"),
	_T("单个室早"),
	_T("室早二联律"),
	_T("室早三联律"),
	_T("心动过速"),
	_T("心动过缓"),
	_T("起搏器未俘获"),
	_T("起搏器未起搏"),
	_T("漏搏")
};

TCHAR* CFMDict::m_dictErrECGLead[] = {
	_T(""),
	_T("ECG LL导联脱落"),
	_T("ECG RL导联脱落"),
	_T("ECG LA导联脱落"),
	_T("ECG RA导联脱落"),
	_T("ECG V 导联脱落")
};

TCHAR* CFMDict::m_dictErrRESP[] = {
	_T(""),
	_T("RESP窒息报警")
};

TCHAR* CFMDict::m_dictErrSPO2[] = {
	_T(""),
	_T("SPO2手指脱落"),
	_T("SPO2搜索脉博"),
	_T("SPO2未接探头"),
	_T("SPO2搜索脉博太长")
};

TCHAR* CFMDict::m_dictErrSPO2Status[] = {
	_T(""),
	_T("SPO2脉博音提示")
};

TCHAR* CFMDict::m_dictErrNIBP[] = {
	_T(""),
	_T("NIBP袖带过松"),
	_T("NIBP漏气"),
	_T("NIBP气压错误"),
	_T("NIBP弱信号"),
	_T("NIBP超范围"),
	_T("NIBP过分运行"),
	_T("NIBP过压"),
	_T("NIBP信号饱和"),
	_T("NIBP漏气检测失败"),
	_T("NIBP系统错误"),
	_T("NIBP超时"),
	_T("NIBP自检失败"),
	_T("NIBP测量失败")
};

TCHAR* CFMDict::m_dictErrTEMP[] = {
	_T(""),
	_T("TEMP通道1脱落"),
	_T("TEMP通道2脱落"),
	_T("TEMP通道1,2脱落")
};

TCHAR* CFMDict::m_dictErrFHR[] = {
	_T(""),
	_T("FHR1探头脱落"),
	_T("FHR2探头脱落")
};

TCHAR* CFMDict::m_dictFHRAutodiagnoAccel[] = {
	_T(""),
	_T("加速"),
	_T("小加速"),
	_T("稽留加速")
};

TCHAR* CFMDict::m_dictFHRAutodiagnoDecel[] = {
	_T(""),
	_T("迟发减速"),
	_T("微小迟发减速"),
	_T("迟发减速尾部延长"),
	_T("变化减速+迟发减速"),
	_T("重变化减速"),
	_T("轻变化减速"),
	_T("早期减速"),
	_T("延长减速"),
	_T("早期减速+变化减速")
};

TCHAR* CFMDict::m_dictDoctorDiagnosis[] = {
	_T(""),
	_T("无刺激试验：反应型"),
	_T("无刺激试验：无反应型"),
	_T("无刺激试验：混合型"),
	_T("宫缩试验：阴性"),
	_T("宫缩试验：阳性"),
	_T("宫缩试验：可疑"),
	_T("宫缩试验：不成功")
};

int CFMDict::GetOnOffCount()
{
	return (sizeof(m_dictOnOff) / sizeof(m_dictOnOff[0]));
}

int CFMDict::GetFHRPaperSpeedOptCount()
{
	return (sizeof(m_dictFHRPaperSpeedOpt) / sizeof(m_dictFHRPaperSpeedOpt[0]));
}

int CFMDict::GetGenderCount()
{
	return (sizeof(m_dictGender) / sizeof(m_dictGender[0]));
}

int CFMDict::GetPatientTypeCount()
{
	return (sizeof(m_dictPatientType) / sizeof(m_dictPatientType[0]));
}

int CFMDict::GetBloodTypeCount()
{
	return (sizeof(m_dictBloodType) / sizeof(m_dictBloodType[0]));
}

int CFMDict::GetAdminTypeCount()
{
	return (sizeof(m_dictAdminType) / sizeof(m_dictAdminType[0]));
}

int CFMDict::GetECGLeadCount()
{
	return (sizeof(m_dictECGLead) / sizeof(m_dictECGLead[0]));
}

int CFMDict::GetECGSpeedCount()
{
	return (sizeof(m_dictECGSpeed) / sizeof(m_dictECGSpeed[0]));
}

int CFMDict::GetECGFilterCount()
{
	return (sizeof(m_dictECGFilter) / sizeof(m_dictECGFilter[0]));
}

int CFMDict::GetECGGainCount()
{
	return (sizeof(m_dictECGGain) / sizeof(m_dictECGGain[0]));
}

int CFMDict::GetNIBPModeCount()
{
	return (sizeof(m_dictNIBPMode) / sizeof(m_dictNIBPMode[0]));
}

int CFMDict::GetNIBPUnitCount()
{
	return (sizeof(m_dictNIBPUnit) / sizeof(m_dictNIBPUnit[0]));
}

int CFMDict::GetTEMPUnitCount()
{
	return (sizeof(m_dictTEMPUnit) / sizeof(m_dictTEMPUnit[0]));
}

int CFMDict::GetFHRReportTypeCount()
{
	return (sizeof(m_dictFHRReportType) / sizeof(m_dictFHRReportType[0]));
}

int CFMDict::GetDoctorMarkCount()
{
	return (sizeof(m_dictDoctorMark) / sizeof(m_dictDoctorMark[0]));
}

int CFMDict::GetECGArrModeCount()
{
	return (sizeof(m_dictECGArrMode) / sizeof(m_dictECGArrMode[0]));
}

int CFMDict::GetErrECGLeadCount()
{
	return (sizeof(m_dictErrECGLead) / sizeof(m_dictErrECGLead[0]));
}

int CFMDict::GetErrRESPCount()
{
	return (sizeof(m_dictErrRESP) / sizeof(m_dictErrRESP[0]));
}

int CFMDict::GetErrSPO2Count()
{
	return (sizeof(m_dictErrSPO2) / sizeof(m_dictErrSPO2[0]));
}

int CFMDict::GetErrSPO2StatusCount()
{
	return (sizeof(m_dictErrSPO2Status) / sizeof(m_dictErrSPO2Status[0]));
}

int CFMDict::GetErrNIBPCount()
{
	return (sizeof(m_dictErrNIBP) / sizeof(m_dictErrNIBP[0]));
}

int CFMDict::GetErrTEMPCount()
{
	return (sizeof(m_dictErrTEMP) / sizeof(m_dictErrTEMP[0]));
}

int CFMDict::GetErrFHRCount()
{
	return (sizeof(m_dictErrFHR) / sizeof(m_dictErrFHR[0]));
}

int CFMDict::GetFHRAutodiagnoAccelCount()
{
	return (sizeof(m_dictFHRAutodiagnoAccel) / sizeof(m_dictFHRAutodiagnoAccel[0]));
}

int CFMDict::GetFHRAutodiagnoDecelCount()
{
	return (sizeof(m_dictFHRAutodiagnoDecel) / sizeof(m_dictFHRAutodiagnoDecel[0]));
}

int CFMDict::GetDoctorDiagnosisCount()
{
	return (sizeof(m_dictDoctorDiagnosis) / sizeof(m_dictDoctorDiagnosis[0]));
}

CString CFMDict::OnOff_itos(int n)
{
	return General_itos(n, m_dictOnOff, GetOnOffCount());
}

int CFMDict::OnOff_stoi(CString s)
{
	return General_stoi(s, m_dictOnOff, GetOnOffCount());
}

CString CFMDict::FHRPaperSpeedOpt_itos(int n)
{
	return General_itos(n, m_dictFHRPaperSpeedOpt, GetFHRPaperSpeedOptCount());
}

int CFMDict::FHRPaperSpeedOpt_stoi(CString s)
{
	return General_stoi(s, m_dictFHRPaperSpeedOpt, GetFHRPaperSpeedOptCount());
}

CString CFMDict::Gender_itos(int n)
{
	return General_itos(n, m_dictGender, GetGenderCount());
}

int CFMDict::Gender_stoi(CString s)
{
	return General_stoi(s, m_dictGender, GetGenderCount());
}

CString CFMDict::PatientType_itos(int n)
{
	return General_itos(n, m_dictPatientType, GetPatientTypeCount());
}

int CFMDict::PatientType_stoi(CString s)
{
	return General_stoi(s, m_dictPatientType, GetPatientTypeCount());
}

CString CFMDict::BloodType_itos(int n)
{
	return General_itos(n, m_dictBloodType, GetBloodTypeCount());
}

int CFMDict::BloodType_stoi(CString s)
{
	return General_stoi(s, m_dictBloodType, GetBloodTypeCount());
}

CString CFMDict::AdminType_itos(int n)
{
	return General_itos(n, m_dictAdminType, GetAdminTypeCount());
}

int CFMDict::AdminType_stoi(CString s)
{
	return General_stoi(s, m_dictAdminType, GetAdminTypeCount());
}

CString CFMDict::ECGLead_itos(int n)
{
	return General_itos(n, m_dictECGLead, GetECGLeadCount());
}

int CFMDict::ECGLead_stoi(CString s)
{
	return General_stoi(s, m_dictECGLead, GetECGLeadCount());
}

CString CFMDict::ECGSpeed_itos(int n)
{
	return General_itos(n, m_dictECGSpeed, GetECGSpeedCount());
}

int CFMDict::ECGSpeed_stoi(CString s)
{
	return General_stoi(s, m_dictECGSpeed, GetECGSpeedCount());
}

CString CFMDict::ECGFilter_itos(int n)
{
	return General_itos(n, m_dictECGFilter, GetECGFilterCount());
}
int CFMDict::ECGFilter_stoi(CString s)
{
	return General_stoi(s, m_dictECGFilter, GetECGFilterCount());
}

CString CFMDict::ECGGain_itos(int n)
{
	return General_itos(n, m_dictECGGain, GetECGGainCount());
}

int CFMDict::ECGGain_stoi(CString s)
{
	return General_stoi(s, m_dictECGGain, GetECGGainCount());
}

CString CFMDict::NIBPMode_itos(int n)
{
	return General_itos(n, m_dictNIBPMode, GetNIBPModeCount());
}

int CFMDict::NIBPMode_stoi(CString s)
{
	return General_stoi(s, m_dictNIBPMode, GetNIBPModeCount());
}

CString CFMDict::NIBPUnit_itos(int n)
{
	return General_itos(n, m_dictNIBPUnit, GetNIBPUnitCount());
}

int CFMDict::NIBPUnit_stoi(CString s)
{
	return General_stoi(s, m_dictNIBPUnit, GetNIBPUnitCount());
}

CString CFMDict::TEMPUnit_itos(int n)
{
	return General_itos(n, m_dictTEMPUnit, GetTEMPUnitCount());
}

int CFMDict::TEMPUnit_stoi(CString s)
{
	return General_stoi(s, m_dictTEMPUnit, GetTEMPUnitCount());
}

CString CFMDict::FHRReportType_itos(int n)
{
	return General_itos(n, m_dictFHRReportType, GetFHRReportTypeCount());
}

int CFMDict::FHRReportType_stoi(CString s)
{
	return General_stoi(s, m_dictFHRReportType, GetFHRReportTypeCount());
}

CString CFMDict::DoctorMark_itos(int n)
{
	return General_itos(n, m_dictDoctorMark, GetDoctorMarkCount());
}

int CFMDict::DoctorMark_stoi(CString s)
{
	return General_stoi(s, m_dictDoctorMark, GetDoctorMarkCount());
}

CString CFMDict::ECGArrMode_itos(int n)
{
	return General_itos(n, m_dictECGArrMode, GetECGArrModeCount());
}

int CFMDict::ECGArrMode_stoi(CString s)
{
	return General_stoi(s, m_dictECGArrMode, GetECGArrModeCount());
}

CString CFMDict::ErrECGLead_itos(int n)
{
	return General_itos(n, m_dictErrECGLead, GetErrECGLeadCount());
}

int CFMDict::ErrECGLead_stoi(CString s)
{
	return General_stoi(s, m_dictErrECGLead, GetErrECGLeadCount());
}

CString CFMDict::ErrRESP_itos(int n)
{
	return General_itos(n, m_dictErrRESP, GetErrRESPCount());
}

int CFMDict::ErrRESP_stoi(CString s)
{
	return General_stoi(s, m_dictErrRESP, GetErrRESPCount());
}

CString CFMDict::ErrSPO2_itos(int n)
{
	return General_itos(n, m_dictErrSPO2, GetErrSPO2Count());
}

int CFMDict::ErrSPO2_stoi(CString s)
{
	return General_stoi(s, m_dictErrSPO2, GetErrSPO2Count());
}

CString CFMDict::ErrSPO2Status_itos(int n)
{
	return General_itos(n, m_dictErrSPO2Status, GetErrSPO2StatusCount());
}

int CFMDict::ErrSPO2Status_stoi(CString s)
{
	return General_stoi(s, m_dictErrSPO2Status, GetErrSPO2StatusCount());
}

CString CFMDict::ErrNIBP_itos(int n)
{
	return General_itos(n, m_dictErrNIBP, GetErrNIBPCount());
}

int CFMDict::ErrNIBP_stoi(CString s)
{
	return General_stoi(s, m_dictErrNIBP, GetErrNIBPCount());
}

CString CFMDict::ErrTEMP_itos(int n)
{
	return General_itos(n, m_dictErrTEMP, GetErrTEMPCount());
}

int CFMDict::ErrTEMP_stoi(CString s)
{
	return General_stoi(s, m_dictErrTEMP, GetErrTEMPCount());
}

CString CFMDict::ErrFHR_itos(int n)
{
	return General_itos(n, m_dictErrFHR, GetErrFHRCount());
}

int CFMDict::ErrFHR_stoi(CString s)
{
	return General_stoi(s, m_dictErrFHR, GetErrFHRCount());
}

CString CFMDict::FHRAutodiagnoAccel_itos(int n)
{
	return General_itos(n, m_dictFHRAutodiagnoAccel, GetFHRAutodiagnoAccelCount());
}

int CFMDict::FHRAutodiagnoAccel_stoi(CString s)
{
	return General_stoi(s, m_dictFHRAutodiagnoAccel, GetFHRAutodiagnoAccelCount());
}

CString CFMDict::FHRAutodiagnoDecel_itos(int n)
{
	return General_itos(n, m_dictFHRAutodiagnoDecel, GetFHRAutodiagnoDecelCount());
}

int CFMDict::FHRAutodiagnoDecel_stoi(CString s)
{
	return General_stoi(s, m_dictFHRAutodiagnoDecel, GetFHRAutodiagnoDecelCount());
}

CString CFMDict::DoctorDiagnosis_itos(int n)
{
	return General_itos(n, m_dictDoctorDiagnosis, GetDoctorDiagnosisCount());
}

int CFMDict::DoctorDiagnosis_stoi(CString s)
{
	return General_stoi(s, m_dictDoctorDiagnosis, GetDoctorDiagnosisCount());
}
