#include "stdafx.h"
#include "FMCenter.h"
#include "PrintFrame.h"
#include "PrintPreviewView.h"


IMPLEMENT_DYNCREATE(CPrintPreviewView, CPreviewView)

CPrintPreviewView::CPrintPreviewView()
{
}

CPrintPreviewView::~CPrintPreviewView()
{
}

BEGIN_MESSAGE_MAP(CPrintPreviewView, CPreviewView)
    //{{AFX_MSG_MAP(CPrintPreviewView)
    ON_COMMAND(AFX_ID_PREVIEW_CLOSE, OnPreviewClose)
    ON_COMMAND(AFX_ID_PREVIEW_PRINT, OnPreviewPrint)
    // NOTE - the ClassWizard will add and remove mapping macros here.
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintPreviewView drawing

void CPrintPreviewView::OnDraw(CDC* pDC)
{    
    CPreviewView::OnDraw(pDC);    
}

/////////////////////////////////////////////////////////////////////////////
// CPrintPreviewView diagnostics

#ifdef _DEBUG
void CPrintPreviewView::AssertValid() const
{
}

void CPrintPreviewView::Dump(CDumpContext& dc) const
{
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPrintPreviewView message handlers

void CPrintPreviewView::OnActivateView(BOOL bActivate, CView* pView1, CView *pView2)
{
    CPreviewView::OnActivateView(bActivate, pView1, pView2);
}

void CPrintPreviewView::OnPreviewClose()
{
	CPrintFrame* pf = theApp.GetPrintMainWnd();
    //theApp.m_pMainWnd = pf->pOldWnd;
    pf->DestroyWindow();
}

void CPrintPreviewView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView)
{
    CPreviewView::OnEndPrintPreview(pDC, pInfo, point, pView);
}

void CPrintPreviewView::OnPreviewPrint()
{
    m_pPrintView->SendMessage(WM_COMMAND, ID_FILE_PRINT);
    OnPreviewClose(); // force close of Preview
}

