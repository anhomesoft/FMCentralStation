#pragma once
#include "afxwin.h"

#define TOOLBAR_TYPE_START                  (1)
#define TOOLBAR_TYPE_PAUSE                  (2)

// CMonitorViewToolbar 对话框
class CFMMonitorView;
class CMonitorViewToolbar : public CDialogEx
{
	DECLARE_DYNAMIC(CMonitorViewToolbar)

private:
	CFMMonitorView* m_pMonitorView;

public:
	CMonitorViewToolbar(CFMMonitorView* pParent);
	virtual ~CMonitorViewToolbar();

// 对话框数据
	enum { IDD = IDD_MONITOR_TOOLBAR };

public:
	void SetStatus(int x, int y, CString type);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CButton m_btnSwitchType;
	afx_msg void OnBnClickedBtnSwitchType();
	afx_msg void OnBnClickedBtnMedicalRecord();
};
