// SetupRESPDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "SetupRESPDlg.h"
#include "afxdialogex.h"
#include "FMSocketThread.h"
#include "FMSocketUnit.h"
#include "FMRecordUnit.h"

// CSetupRESPDlg 对话框

IMPLEMENT_DYNAMIC(CSetupRESPDlg, CDialogEx)

CSetupRESPDlg::CSetupRESPDlg(CWnd* pParent, CFMRecordUnit* pru)
	: CDialogEx(CSetupRESPDlg::IDD, pParent)
	, m_pru(pru)
{
}

CSetupRESPDlg::~CSetupRESPDlg()
{
}

void CSetupRESPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_RR_HIGH, m_edtRRHigh);
	DDX_Control(pDX, IDC_EDIT_RR_LOW, m_edtRRLow);
}

BEGIN_MESSAGE_MAP(CSetupRESPDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CSetupRESPDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSetupRESPDlg::OnBnClickedCancel)
END_MESSAGE_MAP()

// CSetupRESPDlg 消息处理程序
void CSetupRESPDlg::OnBnClickedOk()
{
	// 发送设置
	CString sValue;
	m_edtRRHigh.GetWindowText(sValue);
	int rr_h = _ttoi(sValue);
	m_edtRRLow.GetWindowText(sValue);
	int rr_l = _ttoi(sValue);

	if (rr_h < 6 || rr_h > 120) {
		MessageBox(_T("呼吸率上限超出范围！"));
		m_edtRRHigh.SetFocus();
		return;
	}
	if (rr_l < 6 || rr_l > 120) {
		MessageBox(_T("呼吸率下限超出范围！"));
		m_edtRRLow.SetFocus();
		return;
	}
	if (rr_l >= rr_h) {
		MessageBox(_T("呼吸率下限不能超越上限设定！"));
		m_edtRRLow.SetFocus();
		return;
	}

	CFMSocketUnit* psu = theApp.m_pSocketThread->GetUnit(m_pru->GetIndex());
	psu->SendSetupRESP(rr_h, rr_l);
}

void CSetupRESPDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}

BOOL CSetupRESPDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	DISPLAY_DATA* pdd = m_pru->GetLatestNumbData();
	if (pdd) {
		CString sValue;
		sValue.Format(_T("%d"), pdd->rr_h);
		m_edtRRHigh.SetWindowText(sValue);
		sValue.Format(_T("%d"), pdd->rr_l);
		m_edtRRLow.SetWindowText(sValue);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
