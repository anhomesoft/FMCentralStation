#pragma once

class CFMRecordUnit;
class CFMRecordThread
{
private:
	CFMRecordUnit* m_arUnitGroup[CLIENT_COUNT];
	HANDLE m_hShutdownEvent;
	HANDLE m_arUnitEventGroup[CLIENT_COUNT];
	HANDLE m_hSaveThreadA;
	HANDLE m_hSaveThreadB;

public:
	CFMRecordThread(void);
	virtual ~CFMRecordThread(void);

public:
	inline HANDLE GetShutdownEvent() { return m_hShutdownEvent; };
	inline HANDLE* GetUnitEventGroup() { return m_arUnitEventGroup; };

	BOOL Start();

	CFMRecordUnit* GetUnit(int nIndex);
};

