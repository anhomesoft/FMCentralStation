#ifndef __FM_DICT_H__
#define __FM_DICT_H__

class CFMDict
{
private:
	inline static CString General_itos(int n, TCHAR** dict, int dict_size)
	{
		CString s = _T("");
		if (n < 0 || n > dict_size) {
			return s;
		}
		s = dict[n];
		return s;
	}

	inline static int General_stoi(CString s, TCHAR** dict, int dict_size)
	{
		int i;
		CString sv = TrimAll(s);
		for (i=0; i<dict_size; i++) {
			if (dict[i] == sv) {
				return i;
			}
		}
		return -1;
	}

private:
	static TCHAR* m_dictOnOff[];
	static TCHAR* m_dictFHRPaperSpeedOpt[];
	static TCHAR* m_dictGender[];
	static TCHAR* m_dictPatientType[];
	static TCHAR* m_dictBloodType[];
	static TCHAR* m_dictAdminType[];
	static TCHAR* m_dictECGLead[];
	static TCHAR* m_dictECGSpeed[];
	static TCHAR* m_dictECGFilter[];
	static TCHAR* m_dictECGGain[];
	static TCHAR* m_dictNIBPMode[];
	static TCHAR* m_dictNIBPUnit[];
	static TCHAR* m_dictTEMPUnit[];
	static TCHAR* m_dictFHRReportType[];
	static TCHAR* m_dictDoctorMark[];
	static TCHAR* m_dictECGArrMode[];
	static TCHAR* m_dictErrECGLead[];
	static TCHAR* m_dictErrRESP[];
	static TCHAR* m_dictErrSPO2[];
	static TCHAR* m_dictErrSPO2Status[];
	static TCHAR* m_dictErrNIBP[];
	static TCHAR* m_dictErrTEMP[];
	static TCHAR* m_dictErrFHR[];
	static TCHAR* m_dictFHRAutodiagnoAccel[];
	static TCHAR* m_dictFHRAutodiagnoDecel[];
	static TCHAR* m_dictDoctorDiagnosis[];

public:
	static int GetOnOffCount();
	static int GetFHRPaperSpeedOptCount();
	static int GetGenderCount();
	static int GetPatientTypeCount();
	static int GetBloodTypeCount();
	static int GetAdminTypeCount();
	static int GetECGLeadCount();
	static int GetECGSpeedCount();
	static int GetECGFilterCount();
	static int GetECGGainCount();
	static int GetNIBPModeCount();
	static int GetNIBPUnitCount();
	static int GetTEMPUnitCount();
	static int GetFHRReportTypeCount();
	static int GetDoctorMarkCount();
	static int GetECGArrModeCount();
	static int GetErrECGLeadCount();
	static int GetErrRESPCount();
	static int GetErrSPO2Count();
	static int GetErrSPO2StatusCount();
	static int GetErrNIBPCount();
	static int GetErrTEMPCount();
	static int GetErrFHRCount();
	static int GetFHRAutodiagnoAccelCount();
	static int GetFHRAutodiagnoDecelCount();
	static int GetDoctorDiagnosisCount();

	static CString OnOff_itos(int n);
	static int OnOff_stoi(CString s);
	static CString FHRPaperSpeedOpt_itos(int n);
	static int FHRPaperSpeedOpt_stoi(CString s);
	static CString Gender_itos(int n);
	static int Gender_stoi(CString s);
	static CString PatientType_itos(int n);
	static int PatientType_stoi(CString s);
	static CString BloodType_itos(int n);
	static int BloodType_stoi(CString s);
	static CString AdminType_itos(int n);
	static int AdminType_stoi(CString s);
	static CString ECGLead_itos(int n);
	static int ECGLead_stoi(CString s);
	static CString ECGSpeed_itos(int n);
	static int ECGSpeed_stoi(CString s);
	static CString ECGFilter_itos(int n);
	static int ECGFilter_stoi(CString s);
	static CString ECGGain_itos(int n);
	static int ECGGain_stoi(CString s);
	static CString NIBPMode_itos(int n);
	static int NIBPMode_stoi(CString s);
	static CString NIBPUnit_itos(int n);
	static int NIBPUnit_stoi(CString s);
	static CString TEMPUnit_itos(int n);
	static int TEMPUnit_stoi(CString s);
	static CString FHRReportType_itos(int n);
	static int FHRReportType_stoi(CString s);
	static CString DoctorMark_itos(int n);
	static int DoctorMark_stoi(CString s);
	static CString ECGArrMode_itos(int n);
	static int ECGArrMode_stoi(CString s);
	static CString ErrECGLead_itos(int n);
	static int ErrECGLead_stoi(CString s);
	static CString ErrRESP_itos(int n);
	static int ErrRESP_stoi(CString s);
	static CString ErrSPO2_itos(int n);
	static int ErrSPO2_stoi(CString s);
	static CString ErrSPO2Status_itos(int n);
	static int ErrSPO2Status_stoi(CString s);
	static CString ErrNIBP_itos(int n);
	static int ErrNIBP_stoi(CString s);
	static CString ErrTEMP_itos(int n);
	static int ErrTEMP_stoi(CString s);
	static CString ErrFHR_itos(int n);
	static int ErrFHR_stoi(CString s);
	static CString FHRAutodiagnoAccel_itos(int n);
	static int FHRAutodiagnoAccel_stoi(CString s);
	static CString FHRAutodiagnoDecel_itos(int n);
	static int FHRAutodiagnoDecel_stoi(CString s);
	static CString DoctorDiagnosis_itos(int n);
	static int DoctorDiagnosis_stoi(CString s);
};

#endif