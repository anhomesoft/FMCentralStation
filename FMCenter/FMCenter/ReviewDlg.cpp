// ReviewDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "ReviewDlg.h"
#include "afxdialogex.h"
#include "ReviewECG.h"
#include "ReviewFHR.h"
#include "ReviewTendTable.h"
#include "ReviewTendWave.h"
#include "PrintECG.h"
#include "PrintFHR.h"
#include "PrintTendTable.h"
#include "PrintTendWave.h"
#include "PrintFrame.h"
#include "SelectTendParamDlg.h"

// CReviewDlg 对话框
IMPLEMENT_DYNAMIC(CReviewDlg, CDialogEx)

CReviewDlg::CReviewDlg(CWnd* pParent, CString mrid, int type, CString sStartTime,
	CString sBedNum, CString sMedicalNum, CString sPatientName, CString sPatientGender,
	CString sPatientType, CString sPatientAge, CString sPregnantWeek)
	: CDialogEx(CReviewDlg::IDD, pParent)
{
	m_arBuffer = NULL;
	m_sMRID = mrid;
	m_nType = type;
	m_sStartTime = sStartTime;
	m_tmStart = StringToTime(sStartTime);;
	m_bInitBkDC = FALSE;
	m_pMemDC = NULL;
	m_pPrintFrame = NULL;
	m_nTotalSize = 0;
	m_nPageSpan = 0;
	m_nCurPos = 0;
	m_nOldType = 0;
	m_nPrintType = 0;
	m_nZoomScale = REVIEW_ZOOM_SCALE100;

	m_sBedNum = sBedNum;
	m_sMedicalNum = sMedicalNum;
	m_sPatientName = sPatientName;
	m_sPatientGender = sPatientGender;
	m_sPatientType = sPatientType;
	m_sPatientAge = sPatientAge;
	m_sPregnantWeek = sPregnantWeek;

	m_pECGReviewTool = new CReviewECG(this);
	m_pFHRReviewTool = new CReviewFHR(this);
	m_pTendTableTool = new CReviewTendTable(this);
	m_pTendWaveTool  = new CReviewTendWave(this);

	m_pECGPrintTool       = new CPrintECG(this);
	m_pFHRPrintTool       = new CPrintFHR(this);
	m_pTendTablePrintTool = new CPrintTendTable(this);
	m_pTendWavePrintTool  = new CPrintTendWave(this);

	m_bPrintHR   = theApp.m_bTendPrintHR;
	m_bPrintSYS  = theApp.m_bTendPrintSYS;
	m_bPrintMEA  = theApp.m_bTendPrintMEA;
	m_bPrintDIA  = theApp.m_bTendPrintDIA;
	m_bPrintRR   = theApp.m_bTendPrintRR;
	m_bPrintSPO2 = theApp.m_bTendPrintSPO2;
	m_bPrintT1   = theApp.m_bTendPrintT1;
	m_bPrintT2   = theApp.m_bTendPrintT2;
}

CReviewDlg::~CReviewDlg()
{
	if (m_arBuffer) {
		delete[] m_arBuffer;
		m_arBuffer = NULL;
	}
	if (m_pECGReviewTool) {
		delete m_pECGReviewTool;
		m_pECGReviewTool = NULL;
	}
	if (m_pFHRReviewTool) {
		delete m_pFHRReviewTool;
		m_pFHRReviewTool = NULL;
	}
	if (m_pTendTableTool) {
		delete m_pTendTableTool;
		m_pTendTableTool = NULL;
	}
	if (m_pTendWaveTool) {
		delete m_pTendWaveTool;
		m_pTendWaveTool = NULL;
	}
	if (m_pECGPrintTool) {
		delete m_pECGPrintTool;
		m_pECGPrintTool = NULL;
	}
	if (m_pFHRPrintTool) {
		delete m_pFHRPrintTool;
		m_pFHRPrintTool = NULL;
	}
	if (m_pTendTablePrintTool) {
		delete m_pTendTablePrintTool;
		m_pTendTablePrintTool = NULL;
	}
	if (m_pTendWavePrintTool) {
		delete m_pTendWavePrintTool;
		m_pTendWavePrintTool = NULL;
	}
	if (m_pMemDC) {
		delete m_pMemDC;
		m_pMemDC = NULL;
	}
}

void CReviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, ID_BTN_ECG_WAVEFORM, m_btnECGWaveform);
	DDX_Control(pDX, ID_BTN_FHR_WAVEFORM, m_btnFHRWaveform);
	DDX_Control(pDX, ID_BTN_TEND_TABLE, m_btnTendTable);
	DDX_Control(pDX, ID_BTN_TEND_WAVEFORM, m_btnTendWaveform);
	DDX_Control(pDX, IDC_SCROLLBAR, m_sclBar);
	DDX_Control(pDX, IDC_EDIT_TIME_SPAN, m_edtTimeSpan);
	DDX_Control(pDX, IDC_EDIT_START_TIME, m_edtStartTime);
	DDX_Control(pDX, IDC_EDIT_PATIENT_NAME, m_edtPatientName);
	DDX_Control(pDX, IDC_STATIC_TABLE_AREA, m_staTableArea);
	DDX_Control(pDX, IDC_BTN_ZOOM_OUT, m_btnZoomOut);
	DDX_Control(pDX, IDC_BTN_ZOOM_IN, m_btnZoomIn);
	DDX_Control(pDX, IDC_EDIT_ZOOM_INFO, m_edtZoomInfo);
	DDX_Control(pDX, IDC_COMBO_SELECT_FORMAT, m_cmbSelectFormat);
	DDX_Control(pDX, IDC_STATIC_SELECT_FORMAT, m_staSelectFormat);
	DDX_Control(pDX, IDC_STATIC_SELECT_NIBP_UNIT, m_staSelectNibpUnit);
	DDX_Control(pDX, IDC_COMBO_SELECT_NIBP_UNIT, m_cmbSelectNibpUnit);
	DDX_Control(pDX, IDC_STATIC_SELECT_TEMP_UNIT, m_staSelectTempUnit);
	DDX_Control(pDX, IDC_COMBO_SELECT_TEMP_UNIT, m_cmbSelectTempUnit);
	DDX_Control(pDX, ID_BTN_SELECT_PARAM, m_btnSelectParam);
	DDX_Control(pDX, IDC_COMBO_SELECT_CHANNAL, m_cmbSelectChannel);
	DDX_Control(pDX, IDC_STATIC_DOCTOR_DIAGNO, m_staDoctorDiagno);
	DDX_Control(pDX, IDC_COMBO_DOCTOR_DIAGNO, m_cmbDoctorDiagno);
}

BEGIN_MESSAGE_MAP(CReviewDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CReviewDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CReviewDlg::OnBnClickedCancel)
	ON_BN_CLICKED(ID_BTN_ECG_WAVEFORM, &CReviewDlg::OnBnClickedBtnEcgWaveform)
	ON_BN_CLICKED(ID_BTN_FHR_WAVEFORM, &CReviewDlg::OnBnClickedBtnFhrWaveform)
	ON_BN_CLICKED(ID_BTN_TEND_TABLE, &CReviewDlg::OnBnClickedBtnTendTable)
	ON_BN_CLICKED(ID_BTN_TEND_WAVEFORM, &CReviewDlg::OnBnClickedBtnTendWaveform)
	ON_BN_CLICKED(ID_BTN_PRINT, &CReviewDlg::OnBnClickedBtnPrint)
	ON_WM_PAINT()
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_BTN_ZOOM_OUT, &CReviewDlg::OnBnClickedBtnZoomOut)
	ON_BN_CLICKED(IDC_BTN_ZOOM_IN, &CReviewDlg::OnBnClickedBtnZoomIn)
	ON_CBN_SELCHANGE(IDC_COMBO_SELECT_NIBP_UNIT, &CReviewDlg::OnCbnSelchangeComboSelectNibpUnit)
	ON_CBN_SELCHANGE(IDC_COMBO_SELECT_TEMP_UNIT, &CReviewDlg::OnCbnSelchangeComboSelectTempUnit)
	ON_BN_CLICKED(ID_BTN_SELECT_PARAM, &CReviewDlg::OnBnClickedBtnSelectParam)
END_MESSAGE_MAP()

void CReviewDlg::SetBtnState()
{
	m_btnECGWaveform.SetState(REVIEW_ECG_WAVEFORM == m_nType);
	m_btnFHRWaveform.SetState(REVIEW_FHR_WAVEFORM == m_nType);
	m_btnTendTable.SetState(REVIEW_TEND_TABLE == m_nType);
	m_btnTendWaveform.SetState(REVIEW_TEND_WAVEFORM == m_nType);
}

// CReviewDlg 消息处理程序
BOOL CReviewDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CStringArray sa;
	CUIntArray uaOffset;
	CUIntArray uaSize;
	int count = theApp.GetRecordFileList(sa, m_sMRID, &uaOffset, &uaSize);

	//开辟缓冲区，读入所有记录文件
	m_nTotalSize = ReadRecordFiles(sa, uaOffset, uaSize);

	//更新信息显示
	if (m_sPatientName.IsEmpty()) {
		m_edtPatientName.SetWindowText(_T("----"));
	}
	else {
		m_edtPatientName.SetWindowText(m_sPatientName);
	}

	m_edtStartTime.SetWindowText(m_sStartTime);

	CTimeSpan ts(0, 0, 0, m_nTotalSize);
	m_edtTimeSpan.SetWindowText(ts.Format(_T("%H:%M:%S")));

	int i;
	m_cmbSelectNibpUnit.ResetContent();
	m_cmbSelectTempUnit.ResetContent();
	m_cmbSelectFormat.ResetContent();
	m_cmbDoctorDiagno.ResetContent();
	count = CFMDict::GetNIBPUnitCount();
	for (i=0; i<count; i++) {
		m_cmbSelectNibpUnit.AddString(CFMDict::NIBPUnit_itos(i));
	}
	count = CFMDict::GetTEMPUnitCount();
	for (i=0; i<count; i++) {
		m_cmbSelectTempUnit.AddString(CFMDict::TEMPUnit_itos(i));
	}
	count = CFMDict::GetFHRReportTypeCount();
	for (i=0; i<count; i++) {
		m_cmbSelectFormat.AddString(CFMDict::FHRReportType_itos(i));
	}
	count = CFMDict::GetDoctorDiagnosisCount();
	for (i=0; i<count; i++) {
		m_cmbDoctorDiagno.AddString(CFMDict::DoctorDiagnosis_itos(i));
	}
	m_cmbSelectNibpUnit.SetCurSel(theApp.m_nNIBPUnitOption);
	m_cmbSelectTempUnit.SetCurSel(theApp.m_nTEMPUnitOption);
	m_cmbSelectFormat.SetCurSel(theApp.m_nFHRReportType);
	m_cmbDoctorDiagno.SetCurSel(theApp.GetDoctorDiagno(m_sMRID));

	SetZoomInfoAndButton();
	m_staSelectFormat.ShowWindow(SW_HIDE);
	m_cmbSelectFormat.ShowWindow(SW_HIDE);
	m_edtZoomInfo.ShowWindow(SW_HIDE);
	m_btnZoomOut.ShowWindow(SW_HIDE);
	m_btnZoomIn.ShowWindow(SW_HIDE);
	m_btnSelectParam.ShowWindow(SW_HIDE);
	m_staSelectNibpUnit.ShowWindow(SW_HIDE);
	m_cmbSelectNibpUnit.ShowWindow(SW_HIDE);
	m_staSelectTempUnit.ShowWindow(SW_HIDE);
	m_cmbSelectTempUnit.ShowWindow(SW_HIDE);
	m_cmbSelectChannel.ShowWindow(SW_HIDE);
	m_staDoctorDiagno.ShowWindow(SW_HIDE);
	m_cmbDoctorDiagno.ShowWindow(SW_HIDE);

	SetBtnState();
	SwitchReview();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CReviewDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// 不为绘图消息调用 CDialogEx::OnPaint()
	CRect rc = GetTableRect();

	if (! m_bInitBkDC) {
		m_pMemDC = new CDC();
		m_pMemDC->CreateCompatibleDC(&dc);
		CBitmap bmpMemDC;
		bmpMemDC.CreateCompatibleBitmap(&dc, rc.Width(), rc.Height());
		::DeleteObject(m_pMemDC->SelectObject(&bmpMemDC));
		m_bInitBkDC = TRUE;
	}

	switch (m_nType) {
	case REVIEW_ECG_WAVEFORM:
		m_pECGReviewTool->Draw(m_pMemDC);
		break;
	case REVIEW_FHR_WAVEFORM:
		m_pFHRReviewTool->Draw(m_pMemDC);
		break;
	case REVIEW_TEND_TABLE:
		m_pTendTableTool->Draw(m_pMemDC);
		break;
	case REVIEW_TEND_WAVEFORM:
		m_pTendWaveTool->Draw(m_pMemDC);
		break;
	}

	dc.BitBlt(rc.left, rc.top, rc.Width(), rc.Height(), m_pMemDC, 0, 0, SRCCOPY);
}

int CReviewDlg::GetPageSpan()
{
	switch (m_nType) {
	case REVIEW_ECG_WAVEFORM:  return m_pECGReviewTool->GetPageSpan();
	case REVIEW_FHR_WAVEFORM:  return m_pFHRReviewTool->GetPageSpan();
	case REVIEW_TEND_TABLE:    return m_pTendTableTool->GetPageSpan();
	case REVIEW_TEND_WAVEFORM: return m_pTendWaveTool->GetPageSpan();
	}
	return 1;
}

int CReviewDlg::ReadRecordFiles(CStringArray& sa, CUIntArray& uaOffset, CUIntArray& uaSize)
{
	int i;
	int nTotalSize = 0;
	int count = sa.GetCount();
	for (i=0; i<count; i++) {
		nTotalSize += uaSize[i];

		if (0) {
			CString err;
			err.Format(_T("fname=%s\noffset=%d, size=%d\n"), sa[i], uaOffset[i], uaSize[i]);
			theApp.WriteLog(err);
		}
	}

	BOOL m_bErrPrompt = TRUE;
	try {
		m_arBuffer = new CLIENT_DATA[nTotalSize];
	}
	catch (CMemoryException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CReviewDlg::ReadRecordFiles第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();

		MessageBox(_T("内存不足，不足以打开此记录文件！"));
		nTotalSize = 0;
		return 0;
	}

	for (i=0; i<count; i++) {
		CFile f;
		if (f.Open(sa[i], CFile::modeRead|CFile::typeBinary, NULL)) {
			int offset = uaOffset[i];
			int size = uaSize[i] * sizeof(CLIENT_DATA);

			int real = 0;
			try {
				real = f.Read(&(m_arBuffer[offset]), size);
			}
			catch (CFileException* pe) {
				_TCHAR err[255];
				pe->GetErrorMessage(err, 255);
				theApp.WriteLog(_T("异常位于：CReviewDlg::ReadRecordFiles第2个catch\n"));
				theApp.WriteLog(err);
				theApp.WriteLog(_T("\n"));
				pe->Delete();
			}
			if (m_bErrPrompt && real != size) {
				CString err;
				err.Format(_T("文件损坏：%s\n预期大小：%d，实际大小%d"),
					sa[i], size, real);
				MessageBox(err, _T("错误提示"));
				m_bErrPrompt = FALSE;
			}
			f.Close();
		}
		else {
			if (m_bErrPrompt) {
				CString err;
				err.Format(_T("文件丢失：%s"), sa[i]);
				MessageBox(err, _T("错误提示"));
				m_bErrPrompt = FALSE;
			}
		}
	}
	return nTotalSize;
}

void CReviewDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnOK();
}

void CReviewDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}

void CReviewDlg::SwitchReview()
{
	switch (m_nOldType) {
	case REVIEW_ECG_WAVEFORM:
		m_pECGReviewTool->LeaveReview();
		break;
	case REVIEW_FHR_WAVEFORM:
		m_pFHRReviewTool->LeaveReview();
		break;
	case REVIEW_TEND_TABLE:
		m_pTendTableTool->LeaveReview();
		break;
	case REVIEW_TEND_WAVEFORM:
		m_pTendWaveTool->LeaveReview();
		break;
	}

	switch (m_nType) {
	case REVIEW_ECG_WAVEFORM:
		m_pECGReviewTool->EnterReview();
		break;
	case REVIEW_FHR_WAVEFORM:
		m_pFHRReviewTool->EnterReview();
		break;
	case REVIEW_TEND_TABLE:
		m_pTendTableTool->EnterReview();
		break;
	case REVIEW_TEND_WAVEFORM:
		m_pTendWaveTool->EnterReview();
		break;
	}
	m_nOldType = m_nType;

	//切换模式后，浏览位置应归零
	m_nCurPos = 0;
	RenewScrollBar();
}

void CReviewDlg::OnBnClickedBtnEcgWaveform()
{
	m_nType = REVIEW_ECG_WAVEFORM;
	SetBtnState();
	SwitchReview();
	Invalidate(FALSE);
}

void CReviewDlg::OnBnClickedBtnFhrWaveform()
{
	m_nType = REVIEW_FHR_WAVEFORM;
	SetBtnState();
	SwitchReview();
	Invalidate(FALSE);
}

void CReviewDlg::OnBnClickedBtnTendTable()
{
	m_nType = REVIEW_TEND_TABLE;
	SetBtnState();
	SwitchReview();
	Invalidate(FALSE);
}

void CReviewDlg::OnBnClickedBtnTendWaveform()
{
	m_nType = REVIEW_TEND_WAVEFORM;
	SetBtnState();
	SwitchReview();
	Invalidate(FALSE);
}

CRect CReviewDlg::GetTableRect()
{
	CRect rc;
	m_staTableArea.GetWindowRect(&rc);
	this->ScreenToClient(&rc);
	return rc;
}

void CReviewDlg::InitBkgd(CDC* pdc, COLORREF cr)
{
	CRect rc = GetTableRect();
	rc.OffsetRect(-rc.left, -rc.top);
	CBrush br(cr);
	pdc->FillRect(&rc, &br);
}

CLIENT_DATA* CReviewDlg::GetCurrentBufferPointer(int* pnValidLen)
{
	if (m_nCurPos < 0 || m_nCurPos > m_nTotalSize) {
		return NULL;
	}

	(*pnValidLen) = m_nTotalSize - m_nCurPos;
	return &(m_arBuffer[m_nCurPos]);
}

CTime CReviewDlg::GetCurrentDataTime(int* pnValidLen)
{
	if (m_nCurPos < 0 || m_nCurPos > m_nTotalSize) {
		return 0;
	}

	(*pnValidLen) = m_nTotalSize - m_nCurPos;
	CTimeSpan ts(0, 0, 0, m_nCurPos);
	return (m_tmStart + ts);
}

void CReviewDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	int nOldPos = m_nCurPos;
	int nMaxPos = m_nTotalSize - m_nPageSpan / 2;
	switch (nSBCode) {

	case SB_THUMBTRACK:
		m_nCurPos = nPos;
		break;

	case SB_PAGELEFT:
		m_nCurPos -= m_nPageSpan;
		if (m_nCurPos < 0) {
			m_nCurPos = 0;
		}
		break;

	case SB_PAGERIGHT:
		m_nCurPos += m_nPageSpan;
		if (m_nCurPos > nMaxPos) {
			m_nCurPos = nMaxPos;
		}
		break;

	case SB_LINELEFT:
		if (m_nCurPos > 0) {
			m_nCurPos --;
		}
		break;

	case SB_LINERIGHT:
		if (m_nCurPos < nMaxPos) {
			m_nCurPos ++;
		}
		break;
	}

	pScrollBar->SetScrollPos(m_nCurPos);
	if (m_nCurPos != nOldPos) {
		CRect rc = GetTableRect();
		InvalidateRect(&rc, FALSE);
	}
	//CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}

static void DRWAPRINTER(CDC* pDC, CPrintInfo* pInfo, void* pVoid);
void CReviewDlg::OnBnClickedBtnPrint()
{
	m_pPrintFrame = new CPrintFrame(DRWAPRINTER, this, this, FALSE);
	if (! m_pPrintFrame) {
		return;
	}
	m_nPrintType = m_cmbSelectFormat.GetCurSel();
	switch (m_nType) {
	case REVIEW_ECG_WAVEFORM:  m_pECGPrintTool->InitSetup(); break;
	case REVIEW_FHR_WAVEFORM:  m_pFHRPrintTool->InitSetup(m_nPrintType); break;
	case REVIEW_TEND_TABLE:    m_pTendTablePrintTool->InitSetup(); break;
	case REVIEW_TEND_WAVEFORM: m_pTendWavePrintTool->InitSetup(); break;
	}

	m_pPrintFrame->ShowWindow(SW_SHOW);
	m_pPrintFrame->UpdateWindow();
}

static void DRWAPRINTER(CDC* pDC, CPrintInfo* pInfo, void* pVoid)
{    
	((CReviewDlg *)pVoid)->OnPrint(pDC, pInfo);   
}

void CReviewDlg::OnPrint(CDC * pDC, CPrintInfo* pInfo)
{
	switch (m_nType) {
	case REVIEW_ECG_WAVEFORM:  return m_pECGPrintTool->Print(pDC, pInfo);
	case REVIEW_FHR_WAVEFORM:  return m_pFHRPrintTool->Print(pDC, pInfo);
	case REVIEW_TEND_TABLE:    return m_pTendTablePrintTool->Print(pDC, pInfo);
	case REVIEW_TEND_WAVEFORM: return m_pTendWavePrintTool->Print(pDC, pInfo);
	}
}

void CReviewDlg::SetZoomInfoAndButton()
{
	CString sZoomInfo;
	switch (m_nZoomScale) {
	case REVIEW_ZOOM_SCALE10:  sZoomInfo = _T("显示尺寸10%"); break;
	case REVIEW_ZOOM_SCALE20:  sZoomInfo = _T("显示尺寸20%"); break;
	case REVIEW_ZOOM_SCALE50:  sZoomInfo = _T("显示尺寸50%"); break;
	case REVIEW_ZOOM_SCALE100: sZoomInfo = _T("显示尺寸100%"); break;
	}
	m_edtZoomInfo.SetWindowText(sZoomInfo);
	m_btnZoomOut.EnableWindow(REVIEW_ZOOM_SCALE10 != m_nZoomScale);
	m_btnZoomIn.EnableWindow(REVIEW_ZOOM_SCALE100 != m_nZoomScale);
}

void CReviewDlg::OnBnClickedBtnZoomOut()
{
	if (m_nZoomScale > REVIEW_ZOOM_SCALE10) {
		m_nZoomScale --;
		SetZoomInfoAndButton();
		CRect rc = GetTableRect();
		InvalidateRect(&rc, FALSE);
		RenewScrollBar();
	}
}

void CReviewDlg::OnBnClickedBtnZoomIn()
{
	if (m_nZoomScale < REVIEW_ZOOM_SCALE100) {
		m_nZoomScale ++;
		SetZoomInfoAndButton();
		CRect rc = GetTableRect();
		InvalidateRect(&rc, FALSE);
		RenewScrollBar();
	}
}

void CReviewDlg::OnBnClickedBtnSelectParam()
{
	CRect rc;
	m_btnSelectParam.GetWindowRect(&rc);

	CSelectTendParamDlg dlg(this, rc);
	dlg.DoModal();
}

void CReviewDlg::OnCbnSelchangeComboSelectNibpUnit()
{
	CRect rc = GetTableRect();
	InvalidateRect(&rc, FALSE);
}

void CReviewDlg::OnCbnSelchangeComboSelectTempUnit()
{
	CRect rc = GetTableRect();
	InvalidateRect(&rc, FALSE);
}

void CReviewDlg::RenewScrollBar()
{
	m_nPageSpan = GetPageSpan();
	int nSpan = min(m_nTotalSize, m_nPageSpan);
	SCROLLINFO sbinfo;
	sbinfo.fMask = SIF_ALL;
	sbinfo.nMax  = m_nTotalSize + m_nPageSpan / 2;
	sbinfo.nMin  = 0;
	sbinfo.nPage = nSpan;
	sbinfo.nPos  = m_nCurPos;
	m_sclBar.SetScrollInfo(&sbinfo);
}


