// FMToolbarForm.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "FMToolbarForm.h"
#include "AdvancedSetupDlg.h"
#include "MedicalRecordListDlg.h"
#include "MonitorRecordDlg.h"
#include "TestSelectBedNumDlg.h"
#include "TestSysMetricsDlg.h"
#include "FMRecordUnit.h"


// CFMToolbarForm

IMPLEMENT_DYNCREATE(CFMToolbarForm, CFormView)

CFMToolbarForm::CFMToolbarForm()
	: CFormView(CFMToolbarForm::IDD)
{
	theApp.m_pToolbarForm = this;
	m_nFocusIndex = 0;
	m_nAliveCount = 0;
}

CFMToolbarForm::~CFMToolbarForm()
{
}

void CFMToolbarForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, ID_PAGE1, m_btnPage1);
	DDX_Control(pDX, ID_PAGE2, m_btnPage2);
	DDX_Control(pDX, ID_PAGE3, m_btnPage3);
	DDX_Control(pDX, ID_PAGE4, m_btnPage4);
	DDX_Control(pDX, ID_PAGE5, m_btnPage5);
	DDX_Control(pDX, ID_PAGE6, m_btnPage6);
	DDX_Control(pDX, ID_PAGE7, m_btnPage7);
	DDX_Control(pDX, ID_PAGE8, m_btnPage8);
	DDX_Control(pDX, IDC_CMB_SELECT_BED, m_cmbSelectBed);
	DDX_Control(pDX, ID_ADVANCED_SETUP, m_btnAdvancedSetup);
	DDX_Control(pDX, IDC_EDT_DATETIME, m_edtDatetime);
	DDX_Control(pDX, ID_BTN_SIM_ADD, m_btnSimAdd);
	DDX_Control(pDX, ID_BTN_SIM_DEL, m_btnSimDel);
	DDX_Control(pDX, ID_BTN_SYS_METRICS, m_btnSysMetrics);
	DDX_Control(pDX, IDC_EDT_ALARM_INFO, m_edtAlarmInfo);
}

BEGIN_MESSAGE_MAP(CFMToolbarForm, CFormView)
	ON_BN_CLICKED(IDOK, &CFMToolbarForm::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CFMToolbarForm::OnBnClickedCancel)
	ON_BN_CLICKED(ID_PAGE1, &CFMToolbarForm::OnBnClickedPage1)
	ON_BN_CLICKED(ID_PAGE2, &CFMToolbarForm::OnBnClickedPage2)
	ON_BN_CLICKED(ID_PAGE3, &CFMToolbarForm::OnBnClickedPage3)
	ON_BN_CLICKED(ID_PAGE4, &CFMToolbarForm::OnBnClickedPage4)
	ON_BN_CLICKED(ID_PAGE5, &CFMToolbarForm::OnBnClickedPage5)
	ON_BN_CLICKED(ID_PAGE6, &CFMToolbarForm::OnBnClickedPage6)
	ON_BN_CLICKED(ID_PAGE7, &CFMToolbarForm::OnBnClickedPage7)
	ON_BN_CLICKED(ID_PAGE8, &CFMToolbarForm::OnBnClickedPage8)
	ON_CBN_SELCHANGE(IDC_CMB_SELECT_BED, &CFMToolbarForm::OnCbnSelchangeCmbSelectBed)
	ON_BN_CLICKED(ID_QUIT, &CFMToolbarForm::OnBnClickedQuit)
	ON_BN_CLICKED(ID_MEDICAL_RECORD, &CFMToolbarForm::OnBnClickedMedicalRecord)
	ON_BN_CLICKED(ID_MONITOR_RECORD, &CFMToolbarForm::OnBnClickedMonitorRecord)
	ON_BN_CLICKED(ID_ADVANCED_SETUP, &CFMToolbarForm::OnBnClickedAdvancedSetup)
	ON_BN_CLICKED(ID_BTN_SIM_ADD, &CFMToolbarForm::OnBnClickedBtnSimAdd)
	ON_BN_CLICKED(ID_BTN_SIM_DEL, &CFMToolbarForm::OnBnClickedBtnSimDel)
	ON_WM_TIMER()
	ON_BN_CLICKED(ID_BTN_SYS_METRICS, &CFMToolbarForm::OnBnClickedBtnSysMetrics)
END_MESSAGE_MAP()


// CFMToolbarForm 诊断

#ifdef _DEBUG
void CFMToolbarForm::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CFMToolbarForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void CFMToolbarForm::SetBedButton()
{
	int i;
	CUIntArray ar;
	m_nAliveCount = theApp.GetAliveIndexList(ar);

	//设置combo控件内容
	m_cmbSelectBed.ResetContent();
	for (i=0; i<m_nAliveCount; i++) {
		int nBedNum = ar[i];
		CString sBedNo;
		sBedNo.Format(_T("%02d"), nBedNum + 1);
		int n = m_cmbSelectBed.AddString(sBedNo);
		m_cmbSelectBed.SetItemData(n, (DWORD_PTR)nBedNum);
	}

	//设置分页按钮的显示和隐藏
	m_btnPage1.ShowWindow((m_nAliveCount >  0) ? SW_SHOW : SW_HIDE);
	m_btnPage2.ShowWindow((m_nAliveCount >  8) ? SW_SHOW : SW_HIDE);
	m_btnPage3.ShowWindow((m_nAliveCount > 16) ? SW_SHOW : SW_HIDE);
	m_btnPage4.ShowWindow((m_nAliveCount > 24) ? SW_SHOW : SW_HIDE);
	m_btnPage5.ShowWindow((m_nAliveCount > 32) ? SW_SHOW : SW_HIDE);
	m_btnPage6.ShowWindow((m_nAliveCount > 40) ? SW_SHOW : SW_HIDE);
	m_btnPage7.ShowWindow((m_nAliveCount > 48) ? SW_SHOW : SW_HIDE);
	m_btnPage8.ShowWindow((m_nAliveCount > 56) ? SW_SHOW : SW_HIDE);

	if (m_nFocusIndex >= m_nAliveCount) {
		m_nFocusIndex = m_nAliveCount / 8 * 8;
		m_cmbSelectBed.SetCurSel(m_nFocusIndex);
		switch (m_nFocusIndex) {
		case 0:  PushDownPageBtnState(1); break;
		case 8:  PushDownPageBtnState(2); break;
		case 16: PushDownPageBtnState(3); break;
		case 24: PushDownPageBtnState(4); break;
		case 32: PushDownPageBtnState(5); break;
		case 40: PushDownPageBtnState(6); break;
		case 48: PushDownPageBtnState(7); break;
		case 56: PushDownPageBtnState(8); break;
		}
	}

	if (1 == m_nAliveCount) {
		m_cmbSelectBed.SetCurSel(0);
		PushDownPageBtnState(1);
	}
	else {
		m_cmbSelectBed.SetCurSel(m_nFocusIndex);
	}
}

void CFMToolbarForm::SetComboSel(int nSel)
{
	m_nFocusIndex = nSel;
	m_cmbSelectBed.SetCurSel(nSel);
}

// CFMToolbarForm 消息处理程序
void CFMToolbarForm::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
}

void CFMToolbarForm::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
}

void CFMToolbarForm::PushDownPageBtnState(int n)
{
	m_btnPage1.SetState(1 == n);
	m_btnPage2.SetState(2 == n);
	m_btnPage3.SetState(3 == n);
	m_btnPage4.SetState(4 == n);
	m_btnPage5.SetState(5 == n);
	m_btnPage6.SetState(6 == n);
	m_btnPage7.SetState(7 == n);
	m_btnPage8.SetState(8 == n);
}

void CFMToolbarForm::OnBnClickedPage1()
{
	PushDownPageBtnState(1);
	if (m_nFocusIndex > 7) {
		m_cmbSelectBed.SetCurSel(0);
		m_nFocusIndex = 0;
		theApp.PostLayoutMessage();
	}
}

void CFMToolbarForm::OnBnClickedPage2()
{
	PushDownPageBtnState(2);
	if (m_nFocusIndex < 8 || m_nFocusIndex > 15) {
		m_cmbSelectBed.SetCurSel(8);
		m_nFocusIndex = 8;
		theApp.PostLayoutMessage();
	}
}

void CFMToolbarForm::OnBnClickedPage3()
{
	PushDownPageBtnState(3);
	if (m_nFocusIndex < 16 || m_nFocusIndex > 23) {
		m_cmbSelectBed.SetCurSel(16);
		m_nFocusIndex = 16;
		theApp.PostLayoutMessage();
	}
}

void CFMToolbarForm::OnBnClickedPage4()
{
	PushDownPageBtnState(4);
	if (m_nFocusIndex < 24 || m_nFocusIndex > 31) {
		m_cmbSelectBed.SetCurSel(24);
		m_nFocusIndex = 24;
		theApp.PostLayoutMessage();
	}
}

void CFMToolbarForm::OnBnClickedPage5()
{
	PushDownPageBtnState(5);
	if (m_nFocusIndex < 32 || m_nFocusIndex > 39) {
		m_cmbSelectBed.SetCurSel(32);
		m_nFocusIndex = 32;
		theApp.PostLayoutMessage();
	}
}

void CFMToolbarForm::OnBnClickedPage6()
{
	PushDownPageBtnState(6);
	if (m_nFocusIndex < 40 || m_nFocusIndex > 47) {
		m_cmbSelectBed.SetCurSel(40);
		m_nFocusIndex = 40;
		theApp.PostLayoutMessage();
	}
}

void CFMToolbarForm::OnBnClickedPage7()
{
	PushDownPageBtnState(7);
	if (m_nFocusIndex < 48 || m_nFocusIndex > 55) {
		m_cmbSelectBed.SetCurSel(48);
		m_nFocusIndex = 48;
		theApp.PostLayoutMessage();
	}
}

void CFMToolbarForm::OnBnClickedPage8()
{
	PushDownPageBtnState(8);
	if (m_nFocusIndex < 56) {
		m_cmbSelectBed.SetCurSel(56);
		m_nFocusIndex = 56;
		theApp.PostLayoutMessage();
	}
}

void CFMToolbarForm::OnCbnSelchangeCmbSelectBed()
{
	int nOldFocus = m_nFocusIndex;
	m_nFocusIndex = m_cmbSelectBed.GetCurSel();
	if (nOldFocus == m_nFocusIndex) {
		return;
	}
	if (m_nFocusIndex < 8) { PushDownPageBtnState(1); }
	else if (m_nFocusIndex >  7 && m_nFocusIndex < 16) { PushDownPageBtnState(2); }
	else if (m_nFocusIndex > 15 && m_nFocusIndex < 24) { PushDownPageBtnState(3); }
	else if (m_nFocusIndex > 23 && m_nFocusIndex < 32) { PushDownPageBtnState(4); }
	else if (m_nFocusIndex > 31 && m_nFocusIndex < 40) { PushDownPageBtnState(5); }
	else if (m_nFocusIndex > 39 && m_nFocusIndex < 48) { PushDownPageBtnState(6); }
	else if (m_nFocusIndex > 47 && m_nFocusIndex < 56) { PushDownPageBtnState(7); }
	else if (m_nFocusIndex > 55) { PushDownPageBtnState(8); }
	theApp.PostLayoutMessage();
}

BOOL CFMToolbarForm::QueryQuit()
{
	if (m_nAliveCount > 0) {
		//如果有正在进行中的监护，应提示用户后再退出。
		if (IDOK != MessageBox(_T("您确认要关闭全部监护，然后退出本系统吗？"), _T("退出系统提示"), MB_OKCANCEL)) {
			return FALSE;
		}
	}

	return TRUE;
}

void CFMToolbarForm::OnBnClickedQuit()
{
	if (! QueryQuit()) {
		return;
	}
	//退出系统
	theApp.Close();
}

void CFMToolbarForm::OnBnClickedMedicalRecord()
{
	//病历库
	if (theApp.DebugUI()) {
		MessageBox(_T("当前正处于UI测试模式，不允许访问数据库！"));
		return;
	}
	CMedicalRecordListDlg dlg(this, TYPE_OP_NORMAL);
	dlg.DoModal();
}

void CFMToolbarForm::OnBnClickedMonitorRecord()
{
	//监护记录
	if (theApp.DebugUI()) {
		MessageBox(_T("当前正处于UI测试模式，不允许访问数据库！"));
		return;
	}
	CMonitorRecordDlg dlg;
	dlg.DoModal();
}

void CFMToolbarForm::OnBnClickedAdvancedSetup()
{
	//高级设置
	if (theApp.DebugUI()) {
		MessageBox(_T("当前正处于UI测试模式，不允许访问数据库！"));
		return;
	}
	CAdvancedSetupDlg dlg;
	dlg.DoModal();
}

void CFMToolbarForm::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	if (theApp.DebugUI()) {
		InitTestBedIndex();
		m_edtAlarmInfo.ShowWindow(SW_HIDE);
	}
	else {
		m_btnSimAdd.ShowWindow(SW_HIDE);
		m_btnSimDel.ShowWindow(SW_HIDE);
		m_btnSysMetrics.ShowWindow(SW_HIDE);
	}

	m_btnAdvancedSetup.EnableWindow(1 == theApp.m_nAdminType);
	SetTimer(RENEW_DATETIME_ID, RENEW_DATETIME_SPAN, NULL);
	RenewDatetime();
}

void CFMToolbarForm::CloseTimer()
{
	KillTimer(RENEW_DATETIME_ID);
}

void CFMToolbarForm::OnBnClickedBtnSimAdd()
{
	CTestSelectBedNumDlg dlg(this, TYPE_OP_SIM_ADD);
	if (IDOK == dlg.DoModal()) {
		int nBedIndex = dlg.GetSelectedBedIndex();
		m_test_arAliveBedIndex.Add(nBedIndex);
		int i;
		for (i=0; i<m_test_arSleepBedIndex.GetSize(); i++) {
			if (nBedIndex == m_test_arSleepBedIndex[i]) {
				m_test_arSleepBedIndex.RemoveAt(i);
				break;
			}
		}

		CFMRecordUnit* pu = theApp.GetRecordUnit(nBedIndex);
		if (! pu->Start()) {
			MessageBox(_T("启动失败！"));
		}

		theApp.PostLayoutMessage();
	}
}

void CFMToolbarForm::OnBnClickedBtnSimDel()
{
	CTestSelectBedNumDlg dlg(this, TYPE_OP_SIM_DEL);
	if (IDOK == dlg.DoModal()) {
		int nBedIndex = dlg.GetSelectedBedIndex();
		m_test_arSleepBedIndex.Add(nBedIndex);
		int i;
		for (i=0; i<m_test_arAliveBedIndex.GetSize(); i++) {
			if (nBedIndex == m_test_arAliveBedIndex[i]) {
				m_test_arAliveBedIndex.RemoveAt(i);
				break;
			}
		}

		CFMRecordUnit* pu = theApp.GetRecordUnit(nBedIndex);
		pu->Stop();

		theApp.PostLayoutMessage();
	}
}

void CFMToolbarForm::InitTestBedIndex()
{
	m_test_arAliveBedIndex.RemoveAll();
	m_test_arSleepBedIndex.RemoveAll();
	int i;
	for (i=0; i<CLIENT_COUNT; i++) {
		m_test_arSleepBedIndex.Add(i);
	}
}

void CFMToolbarForm::OnTimer(UINT_PTR nIDEvent)
{
	//更新时间显示
	RenewDatetime();
	CFormView::OnTimer(nIDEvent);
}

void CFMToolbarForm::RenewDatetime()
{
	CTime tmNow = CTime::GetCurrentTime();
	CString sNow = tmNow.Format(_T(" %Y-%m-%d %H:%M:%S"));
	m_edtDatetime.SetWindowTextW(sNow);
}

void CFMToolbarForm::OnBnClickedBtnSysMetrics()
{
	CTestSysMetricsDlg dlg;
	dlg.DoModal();
}
