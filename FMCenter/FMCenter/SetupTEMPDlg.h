#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CSetupTEMPDlg 对话框
class CFMRecordUnit;
class CSetupTEMPDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSetupTEMPDlg)

private:
	CFMRecordUnit* m_pru;
	int m_nTEMPUnitOpt;

public:
	CSetupTEMPDlg(CWnd* pParent, CFMRecordUnit* pru, int nTEMPUnit);
	virtual ~CSetupTEMPDlg();

	inline int GetTEMPUnitOpt() { return m_nTEMPUnitOpt; };

// 对话框数据
	enum { IDD = IDD_DIALOG_SETUP_TEMP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CEdit m_edtT1High;
	CEdit m_edtT1Low;
	CEdit m_edtT2High;
	CEdit m_edtT2Low;
	CComboBox m_cmbTEMPUnit;
	CStatic m_staRange1;
	CStatic m_staRange2;
	CStatic m_staRange3;
	CStatic m_staRange4;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedSend();
};
