use master --切换到master数据库
GO
--检测是否存在同名的数据库 
IF exists(SELECT 1 FROM sysdatabases WHERE name = 'FMServerDB') 
 DROP DATABASE FMServerDB
GO

CREATE DATABASE FMServerDB
ON --数据文件
(
 name = 'fmserver_mdf', --数据文件逻辑名
 filename = 'E:\FMDATA\FMServerDB.mdf',--数据文件存放路径
 size = 10MB,--初始大小
 maxsize = 200MB,--最大大小
 filegrowth = 10MB--增长速度
)
log ON --日志文件
(
 name = 'tour_ldf', --日志文件逻辑名
 filename = 'E:\FMDATA\FMServerDB.ldf',--日志文件存放路径
 size = 1MB,--初始大小
 maxsize = 10MB,--最大大小
 filegrowth = 1MB--增长速度
)
GO

USE FMServerDB;
GO
-- 删除已经存在的表
IF OBJECT_ID('system_setup') is not null
 DROP TABLE system_setup;
IF OBJECT_ID('operator_info') is not null
 DROP TABLE operator_info;
IF OBJECT_ID('file_record') is not null
 DROP TABLE file_record;
IF OBJECT_ID('monitor_record') is not null
 DROP TABLE monitor_record;
IF OBJECT_ID('medical_record') is not null
 DROP TABLE medical_record;

GO

-- 病历
CREATE TABLE medical_record (
 medical_num nvarchar(24) NOT NULL PRIMARY KEY,
 patient_name nvarchar(24) NOT NULL,
 height int NOT NULL,
 weight int NOT NULL,
 gender int NOT NULL,
 bloodtype int NOT NULL,
 patient_type int NOT NULL,
 doctor nvarchar(24) NOT NULL,
 clinic nvarchar(24) NOT NULL,
 address nvarchar(64) NOT NULL,
 phone_number nvarchar(24) NOT NULL,
 birthday smalldatetime NOT NULL,
 admission_date smalldatetime NOT NULL,
 create_date smalldatetime NOT NULL,
 operator nvarchar(24) NOT NULL
) ON [PRIMARY]
GO

-- 监护记录
CREATE TABLE monitor_record (
 mrid int NOT NULL PRIMARY KEY,
 bed_num int NOT NULL,
 create_date datetime NOT NULL,
 operator nvarchar(24) NOT NULL,
 med_num nvarchar(24),
 pregnant_week nvarchar(10),
 doctor_diagno int,
 CONSTRAINT med_num_foreign
 FOREIGN KEY (med_num) REFERENCES medical_record(medical_num)
) ON [PRIMARY]
GO

-- 监护文件信息
CREATE TABLE file_record (
 fid int NOT NULL PRIMARY KEY IDENTITY(1,1),
 foffset int NOT NULL,
 fsize int NOT NULL,
 fname nvarchar(260) NOT NULL,
 mrid int,
 CONSTRAINT mrid_foreign
 FOREIGN KEY (mrid) REFERENCES monitor_record(mrid)
) ON [PRIMARY]
GO

-- 管理员信息
CREATE TABLE operator_info (
 oid int NOT NULL PRIMARY KEY IDENTITY(1,1),
 otype int NOT NULL,
 oname nvarchar(24) NOT NULL,
 opass nvarchar(24) NOT NULL,
) ON [PRIMARY]
GO

-- 系统设置
CREATE TABLE system_setup (
 sid int NOT NULL PRIMARY KEY,
 hospital_name nvarchar(48) NOT NULL,
 fhr_speed_option int NOT NULL,
 nibp_unit_option int NOT NULL,
 temp_unit_option int NOT NULL,
 fhr_report_type int NOT NULL,
 tend_print_setup nvarchar(8) NOT NULL,
) ON [PRIMARY]
GO

select * from medical_record;
select * from monitor_record;
select * from file_record;
select * from operator_info;
select * from system_setup;
GO
