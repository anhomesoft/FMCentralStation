#pragma once
#include "afxwin.h"
#include "PrintPreviewView.h"
#include "PrintFrame.h"

class CPrintWrapperView : public CScrollView
{
public:
	CPrintWrapperView(void);
	virtual ~CPrintWrapperView(void);

protected:
	DECLARE_DYNCREATE(CPrintWrapperView)
	CFrameWnd*    m_pFrameWnd;

public:
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	void OnFilePrintPreview(CFrameWnd *pFrame);
	BOOL DoPrintPreview(UINT nIDResource, CView* pPrintView,CRuntimeClass* pPreviewViewClass, CPrintPreviewState* pState);
	void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo,POINT, CPrintPreviewView* pView);
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintWrapperView)

protected:
	virtual void OnPrint(CDC* pDC,CPrintInfo* pInfo);      // overridden to draw this view
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	//}}AFX_VIRTUAL
	// Implementation
protected:

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CPrintWrapperView)
	// NOTE - the ClassWizard will add and remove member functions here.
	afx_msg void OnFilePrint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

