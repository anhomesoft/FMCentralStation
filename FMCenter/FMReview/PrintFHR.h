#pragma once
#include "reviewdlg.h"

class CFHRAutodiagnostics;
class CPrintFHR : public CPrintTool
{
public:
	CPrintFHR(CReviewDlg* pdlg);
	virtual ~CPrintFHR(void);

public:
	virtual void InitSetup(int type=0);
	virtual void Print(CDC* pdc,  CPrintInfo* pInfo);

private:
	int m_type;
	CFHRAutodiagnostics* m_pAuto;
	CFont m_fontTableScale;
	CFont m_fontFhrInfo;
	CFont m_fontDoctorSign;
	CFont* m_pOldFont;
	int m_nDoctorDiagno;
	int m_nMoveCount;

private:
	void Autodiagnostics(void);
	void SelectScaleFont(CDC* pdc);
	void SelectInfoFont(CDC* pdc);
	void SelectDoctorSignFont(CDC* pdc);
	void ResetFont(CDC* pdc);
	void DrawGridF10(CDC* pdc, CRect& rc);
	void DrawGridF20(CDC* pdc, CRect& rc);
	void DrawTimeF10(CDC* pdc, CRect& rc);
	void DrawTimeF20(CDC* pdc, CRect& rc);
	void DrawWaveF10(CDC* pdc, CRect& rc);
	void DrawWaveF20(CDC* pdc, CRect& rc);
	void DrawInfo(CDC* pdc, CRect& rc);
	void DrawReport(CDC* pdc, CRect& rc);
	void DrawTimeRuler(CDC* pdc, int x, int y, CTime tm, int h, int nSeconds);
	void DrawFMMark(CDC* pdc, COLORREF cr, int x, int y, int h);
	void DrawDiagnosisFlag(CDC* pdc, int x, int y, int type);
};

