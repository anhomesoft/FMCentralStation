#pragma once
#include "afxpriv.h"
class CPrintPreviewView : public CPreviewView
{
protected:
    CPrintPreviewView();           // protected constructor used by dynamic creation
    virtual ~CPrintPreviewView();
    DECLARE_DYNCREATE(CPrintPreviewView)

public:
    friend class CWrapperView;
	friend class CPrintWrapperView;

// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CPrintPreviewView)
    protected:
    virtual void OnDraw(CDC* pDC);      // overridden to draw this view
    virtual void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
    //}}AFX_VIRTUAL

// Implementation
protected:

#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
#endif

    virtual void OnActivateView(BOOL bActivate,CView* pActivateView, CView* pDeactiveView);

    // Generated message map functions
    //{{AFX_MSG(CPrintPreviewView)
    afx_msg void OnPreviewClose();
    afx_msg void OnPreviewPrint();
        // NOTE - the ClassWizard will add and remove member functions here.
    //}}AFX_MSG

    friend BOOL CALLBACK _AfxMyPreviewCloseProc(CFrameWnd* pFrameWnd);

    DECLARE_MESSAGE_MAP()
};

